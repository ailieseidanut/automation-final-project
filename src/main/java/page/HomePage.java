package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    private String URL = "http://automationpractice.com/index.php"
    private String title = "My Store";

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/a")
    private WebElement woman;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]/a")
    private WebElement dress;

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[3]/a")
    private WebElement tShort;

    @FindBy(xpath = "//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a")
    private WebElement cart;

    @FindBy(xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")
    private WebElement login;

    @FindBy(xpath = "//*[@id=\"contact-link\"]/a")
    private WebElement contactUs;

    @FindBy(xpath = "//*[@id=\"homeslider\"]/li[4]/a")
    private WebElement homeSliderContain;

    @FindBy(id = "search_query_top")
    private WebElement searchField;

    @FindBy(xpath = "//*[@id=\"home-page-tabs\"]/li[1]/a")
    private WebElement popular;


    public void clickOnWomanButton(){
        woman.click();
    }
    public void clickOnDressButton(){
        dress.click();
    }
    public void clickOnTshortCategory(){
        tShort.click();
    }
    public void clickOnChar(){
        cart.click();
    }
    public void clickOnLogin(){
        login.click();
    }
    public void clickOnContactUs(){
        contactUs.click();
    }











}
